/* Name - Pratyush Swamy
** Unity ID - pswamy
** Sub - CSC541
** Assignment - IV
** Binary Search Tree */
#include <stdlib.h>
#include <string>
#include <fstream>
#include <iostream>
#include<ctime>

using namespace std;
string fileName;

//Struct for Node
struct binary_node{
	int key;
	long left;
	long right;
};

struct queue{
	queue * next;
	binary_node * node;
	long offset;
};

queue * front = 0;
queue * back = 0;



//Get A Node
binary_node *loadNode(long offset){
	fstream readNode;
	binary_node *node = new binary_node;

	readNode.open(fileName, ios::in|ios::out|ios::binary);
	readNode.seekg(0, ios::end);
	int seek = readNode.tellg();
	if(seek == 0){
		readNode.close();
		return 0;
	}
	else{
		readNode.seekg(offset, ios::beg);
		readNode.read((char *)&node->key,sizeof(int));
		readNode.seekg(offset+sizeof(int),ios::beg);
		readNode.read((char *)&node->left,sizeof(long));
		readNode.seekg(offset+sizeof(int)+sizeof(long), ios::beg);
		readNode.read((char *)&node->right, sizeof(long));
		readNode.close();
		return node;
	}
}


//Write the new offset of the left/right child node
void writeParentNode(long offset, string child_node){
	fstream writer;
	//cout << "Updating Parent Node!!" << endl;
	writer.open(fileName, ios::in|ios::out|ios::binary);
	writer.seekp(0, ios::end);
	long child = writer.tellp();

	if(child_node == "left"){
		//cout << "Updating the left offset of Parent!!!" << endl;
		writer.seekp((offset + sizeof(int)), ios::beg);
		writer.write((char *)&child, sizeof(long));
	}
	else if(child_node == "right"){
		//cout << "Updating the Right offset of Parent!!" << endl;
		writer.seekp(offset + sizeof(int) + sizeof(long), ios::beg);
		writer.write((char *)&child, sizeof(long));
	}
	writer.close();
	return;
}

//Function to add the data to the File
void writeNode(int newKey){
	fstream writer;
	//cout << "In the Write Function!!" << endl;
	int children = -1;

	writer.open(fileName, ios::in|ios::out|ios::binary);
	writer.seekp(0,ios::end);
	writer.write((char *)&newKey, sizeof(int));
	writer.write((char *)&children, sizeof(long));
	writer.write((char *)&children, sizeof(long));
	writer.close();
}

//Inserting the Node in the Binary Tree
void insert_node(int newKey, binary_node *node, long offset){
	//Root Condition
	if(node == 0){
		writeNode(newKey);
		return;
	}

		if(node->key == newKey){
		//cout << newKey << " already exists!!!!" << endl;
		return;
	}
	//Searching Left Subtree
	else if(newKey < node->key){
		if(node->left == -1){
			string child = "left";
			writeParentNode(offset, child);
			writeNode(newKey);
			return;
		}
		else
			insert_node(newKey, loadNode(node->left), node->left);
	}
	// Searching Right Subtree
	else {
		if(node->right == -1){
			string child = "right";
			writeParentNode(offset, child);
			writeNode(newKey);
			return;
		}
		else
			insert_node(newKey, loadNode(node->right), node->right);
	}
	
}

//Sending the Keys from the front of the queue - Dequeue
queue * dequeue(){
	queue * tempKey = front;
	if ( front != 0 )
		front = front->next;
	if(back == tempKey) 
		back =0;
	return tempKey;
}

//Inserting Keys at the End of the Queue
void enqueue(binary_node * node, long offset){
	queue * tempKey = new queue();
	tempKey->node = node;
	tempKey->next = 0;
	tempKey->offset = offset;

	if(front == 0 && back == 0){
		front = tempKey;
		back = tempKey;
	}
	else {
		back ->next = tempKey;
		back = tempKey;
	}
}

//Final Print 

void printLevelOrder(){

	int currentLevelNodes = 1;
	int nextLevelNodes = 0;
	int level = 1;

	enqueue(loadNode(0), 0);
	cout << level << ": ";
	while (front != 0 && back != 0) {
		queue * newNode = dequeue();
		currentLevelNodes--;
		
		//if (newNode) {
			
			cout << newNode->node->key << "/" <<newNode->offset << " ";
			if(newNode->node->left != -1){
				enqueue(loadNode(newNode->node->left), newNode->node->left);
				nextLevelNodes ++;
			}
			if(newNode->node->right != -1){
				enqueue(loadNode(newNode->node->right), newNode->node->right);
				nextLevelNodes ++;
			}
		//}
		if (currentLevelNodes == 0) {
			cout << endl;
			
			level++;
			currentLevelNodes = nextLevelNodes;
			if(nextLevelNodes != 0){
				cout << level << ": ";
			}
			nextLevelNodes = 0;
			
		}
	}
}

void search(int newKey, binary_node *node){
	//Root Condition
	if(node == 0){
		cout << "Empty Tree." << endl;
		return;
	}

	if(node->key == newKey){
		cout <<"Record " << newKey << " exists." << endl;
		return;
	}
	//Searching Left Subtree
	else if(newKey < node->key){
		if(node->left == -1){
			cout << "Record " << newKey << " does not exist." << endl;
			return;
		}
		else
			search(newKey, loadNode(node->left));
	}
	// Searching Right Subtree
	else {
		if(node->right == -1){
			cout << "Record " << newKey << " does not exist." << endl;
			return;
		}
		else
			search(newKey, loadNode(node->right));
	}
	
}
//Main
int main (int argc, char* argv[] ){
	fileName = argv[1];
	string input="";
	int key;
	float duration = 0;
	int count = 0;
	clock_t start = 0;
	//Deleting File
	string command = "del "+fileName;
	const char *deleteCommand = command.c_str();
	system(deleteCommand);
	//-------------------------------------//
	
	fstream stream;
	stream.open(fileName, ios::app);
	stream.close();

	while(cin >> input){
		if(input == "add"){
			int key;
			cin >> key;
			insert_node(key, loadNode(0), 0);
		}
		else if(input == "find"){
			int key;
			cin >> key;
			count++;
			start = clock();
			search(key, loadNode(0));
			duration+=((std::clock()-start)/(float)CLOCKS_PER_SEC);
		}
		else if(input == "print"){
			printLevelOrder();
		}
		else if(input == "end"){
		cout << endl;
		cout << "Sum: " << duration <<endl;
		cout << "Avg: " << duration/count << endl;
		return 0;
		}
	}
}

