/* Name - Pratyush Swamy
** Unity ID - pswamy
** Sub - CSC541
** Assignment - IV
** B Tree Implementation*/
#include "BTreeNode.h"

// Constructor for BTreeNode class
BTreeNode::BTreeNode(int t1, long _offset, char *file)
{
	int  i = 0;
	// Copy the given minimum degree and leaf property
	t = t1;
	// Allocate memory for maximum number of possible keys
	// and child pointers
	keys = new int[2*t];
	C = new long[2*t+1];

	for(; i <2*t; i++){
		keys[i] = -1;
		C[i] = -1;
	}
	C[i] = -1; // i = 2t
	// Initialize the number of keys as 0
	n = 0;
	offset = _offset;
	fileName = file;
	readNode();
	leaf = isLeaf();
}



bool BTreeNode::isLeaf(){
	int  i =0;
	for(;i<=2*t;i++){
		if(C[i] != -1) return false;
	}
	return true;
}

void  BTreeNode::readNode(){
	fstream stream;
	int  i =0;
	stream.open( fileName, ios::in|ios::out|ios::binary);
	stream.seekg(0,ios::end);
	int seek = stream.tellg();
	stream.close();
	if(seek == 0 || offset > seek){
		isValid = false;
		return;
	}
	if(offset == seek){
		writeNode();
		return;
	}
	stream.open( fileName, ios::in|ios::out|ios::binary);
	stream.seekg(offset, ios::beg);
	stream.read((char *)&n, sizeof(int));
	int tempOffset = offset ;
	for(i=0;i<n;i++){
		tempOffset += sizeof(int);
		stream.seekg(tempOffset, ios::beg);
		stream.read((char *)&C[i], sizeof(int));
		tempOffset += sizeof(int);
		stream.seekg(tempOffset, ios::beg);
		stream.read((char *)&keys[i], sizeof(int));
	}
	tempOffset += sizeof(int);
	stream.seekg(tempOffset, ios::beg); // red the last child
	stream.read((char *)&C[i], sizeof(int));
	stream.close();
	isValid =   true;
}

void BTreeNode::writeNode(){
	fstream stream;
	int  i =0;
	// need to write a new node to the file
		stream.open( fileName, ios::in|ios::out|ios::binary);
		stream.seekp(offset, ios::beg);
		stream.write((char *)&n, sizeof(int));
		for(i = 0; i < 2*t;i++){
			stream.write((char *)&C[i], sizeof(int));
			stream.write((char *)&keys[i], sizeof(int));
		}
		stream.write((char *)&C[i], sizeof(int)); // i = 2t
		stream.close();
}

bool BTreeNode::isNodeValid(){
	 return isValid;
}
 
// Function to traverse all nodes in a subtree rooted with this node
void BTreeNode::traverse()
{
	// There are n keys and n+1 children, travers through n keys
	// and first n children
	int i;
	for (i = 0; i < n; i++)
	{
		// If this is not leaf, then before printing key[i],
		// traverse the subtree rooted with child C[i].
		if (leaf == false)
			//C[i]->traverse();
		cout << " " << keys[i];
	}
 
	// Print the subtree rooted with last child
	if (leaf == false);
	//	C[i]->traverse();
}
 
// Function to search key k in subtree rooted with this node
BTreeNode *BTreeNode::search(int k)
{
	// Find the first key greater than or equal to k
	int i = 0;
	while (i < n && k > keys[i])
		i++;
 
	// If the found key is equal to k, return this node
	if (keys[i] == k)
		return this;
 
	// If key is not found here and this is a leaf node
	if (leaf == true)
		return NULL;
 
	// Go to the appropriate child
	BTreeNode *child = new BTreeNode(t, C[i], fileName);
	return child->search(k);
	
}

// A utility function to insert a new key in this node
// The assumption is, the node must be non-full when this
// function is called
void BTreeNode::insertNonFull(int k)
{
	// Initialize index as index of rightmost element
	int i = n-1;
 
	// If this is a leaf node
	if (leaf == true)
	{
		// The following loop does two things
		// a) Finds the location of new key to be inserted
		// b) Moves all greater keys to one place ahead
		while (i >= 0 && keys[i] > k)
		{
			keys[i+1] = keys[i];
			i--;
		}
 
		// Insert the new key at found location
		keys[i+1] = k;
		n = n+1;
		writeNode();
	}
	else // If this node is not leaf
	{
		// Find the child which is going to have the new key
		while (i >= 0 && keys[i] > k)
			i--;
		BTreeNode *child = new BTreeNode(t, C[i+1], fileName);
		// See if the found child is full
		if (child->n == 2*t)
		{
			// If the child is full, then split it
			splitChild(i+1, child);
 
			// After split, the middle key of C[i] goes up and
			// C[i] is splitted into two.  See which of the two
			// is going to have the new key
			if (keys[i+1] < k)
				i++;
		}
		child = new BTreeNode(t, C[i+1], fileName);
		child->insertNonFull(k);
		child->writeNode();
	}
}
 
// A utility function to split the child y of this node
// Note that y must be full when this function is called
void BTreeNode::splitChild(int i, BTreeNode *y)
{
	// Create a new node which is going to store (t-1) keys
	// of y
	fstream stream;
	stream.open( fileName, ios::in|ios::out|ios::binary);
	stream.seekg(0,ios::end);
	int seek = stream.tellg();
	stream.close();
	BTreeNode *z = new BTreeNode(y->t, seek, fileName);
	z->n = t - 1;
 
	// Copy the last (t-1) keys of y to z
	for (int j = 0; j < t-1; j++)
		z->keys[j] = y->keys[j+t+1];
 
	// Copy the last t children of y to z
	if (y->leaf == false)
	{
		for (int j = 0; j < t; j++)
			z->C[j] = y->C[j+t+1];
	}
 
	// Reduce the number of keys in y
	y->n = t ;
 
	// Since this node is going to have a new child,
	// create space of new child
	for (int j = n; j >= i+1; j--)
		C[j+1] = C[j];
 
	// Link the new child to this node
	C[i+1] = z->offset;
 
	// A key of y will move to this node. Find location of
	// new key and move all greater keys one space ahead
	for (int j = n-1; j >= i; j--)
		keys[j+1] = keys[j];
 
	// Copy the middle key of y to this node
	keys[i] = y->keys[t];
 
	// Increment count of keys in this node
	n = n + 1;
	z->writeNode();
	y->writeNode();
	writeNode();
}