/* Name - Pratyush Swamy
** Unity ID - pswamy
** Sub - CSC541
** Assignment - IV
** B Tree Implementation*/
#ifndef _BTREE_H_
#define _BTREE_H_
#include  "BTreeNode.h"
// A BTree
class BTree
{
	BTreeNode *root; // Pointer to root node
	int t;  // Minimum degree
	char *fileName;
	long rootOffset;

	struct printNode{
		BTreeNode *data;
		printNode *next ;
		printNode *head ;
		printNode *tail;

		printNode(){
			data = 0;
			head = 0;
			tail = 0;
		}

		void push(BTreeNode *node){
			if(node->n != 0){
				printNode *newNode = new printNode();
				newNode->data = node;
				newNode->next  = 0;
				if(head == 0)
					head = newNode;
				if(tail != 0 ) tail->next = newNode;
				tail = newNode;
			}
		}

		bool isEmpty(){
			return head  == 0;
		}

		printNode* pop(){
			printNode *temp = head;
			if(temp == 0) return 0;
			head = head->next;
			if(temp == tail) tail = tail->next;
			temp->next = 0;
			return temp;
		} 

		void print(){
			for(int i =0; i< data->n;i++){
				cout<<data->keys[i];
				if(i != data->n -1) cout<<",";
			}
			cout<<"/"<<data->offset<<" ";
		}
	};
public:
	// Constructor (Initializes tree as empty)
	BTree(int _t, char *file)
	{   root = NULL;  
		t = _t; 
		fileName = file;
		rootOffset = 0;
	}
 
	// function to traverse the tree
	void traverse();
	
 
	// function to search a key in this tree
	BTreeNode* search(int k)
	{  
		root = new BTreeNode(t, rootOffset, fileName);
		return !root->isNodeValid() ? NULL : 
		root->search(k); 
	}
 
	// The main function that inserts a new key in this B-Tree
	void insert(int k);
};
#endif