/* Name - Pratyush Swamy
** Unity ID - pswamy
** Sub - CSC541
** Assignment - IV
** B Tree Implementation*/
#include "BTree.h"
// The main function that inserts a new key in this B-Tree
void BTree::insert(int k)
{
	root = new BTreeNode(t, rootOffset, fileName);
	// If tree is empty
	if (!root->isNodeValid())
	{
		// Allocate memory for root
		
		root->keys[0] = k;  // Insert key
		root->n = 1;  // Update number of keys in root
		root->writeNode();
	}
	else // If tree is not empty
	{
		// If root is full, then tree grows in height
		if (root->n == 2*t)
		{
			fstream stream;
			stream.open( fileName, ios::in|ios::out|ios::binary);
			stream.seekg(0,ios::end);
			int seek = stream.tellg();
			stream.close();
			// Allocate memory for new root
			BTreeNode *s = new BTreeNode(t, seek, fileName);
			//s->writeNode();
			//root->writeNode();
			// Make old root as child of new root
			s->C[0] = root->offset;
 
			// Split the old root and move 1 key to the new root
			s->splitChild(0, root);
			s->writeNode();
			root->writeNode();
			// New root has two children now.  Decide which of the
			// two children is going to have new key
			int i = 0;
			if (s->keys[0] < k)
				i++;
			BTreeNode child(t, s->C[i], fileName);
			child.insertNonFull(k);
			child.writeNode();
			// Change root
			//root = s;
			rootOffset = s->offset;
		}
		else  // If root is not full, call insertNonFull for root
			root->insertNonFull(k);
	}
}

void BTree::traverse(){
	root = new BTreeNode(t, rootOffset, fileName);
	if(root->isNodeValid()){
		int level = 1;
		printNode q1, q2;
		q1.push(root);
		cout<<level<<":"<<" ";
		while(!q1.isEmpty()){
			printNode *node = q1.pop();
			node->print();
			for(int i=0;i<=root->n;i++){
				if(node->data->C[i] != -1){
					BTreeNode *temp = new BTreeNode(t, node->data->C[i], fileName);
					q2.push(temp);
				}
			}
			delete node;
			if(q1.isEmpty()){
				if(!q2.isEmpty()) cout<<"\n"<<(++level)<<":"<<" ";
				while(!q2.isEmpty()) q1.push(q2.pop()->data);
			}
		}
		cout<<"\n";
	}
}