/* Name - Pratyush Swamy
** Unity ID - pswamy
** Sub - CSC541
** Assignment - IV
** B Tree Implementation*/
// Driver program to test above functions
#include <iostream>
#include <string>
#include "BTree.h"
#include <ctime>
using namespace std;
int count = 0;
int main(int argc, char *argv[])
{

	if(argc != 2){
		cout<<"Usage:bst <filename>";
		return 1;
	}
	char *filename = argv[1];
	string command = "del ";
	command.append(filename);
	system( command.c_str());
	fstream stream;
	stream.open(filename,ios::app);
	stream.close();
	float duration;
	BTree t(16, filename); // A B-Tree with minium degree 16
	string input="";
	int val;
	while(cin>>input){
		if(input == "add"){
			cin>>val;
			t.insert(val);
		}
		else if(input == "find"){
			cin>>val;
			float start = 0;
			count++;
			start = clock();
			BTreeNode *node  = t.search(val);
			if(node != NULL)
				cout<<"Record "<<val<<" exists.\n";
			else cout<<"Record "<<val<<" does not exist.\n";
			duration+=((std::clock()-start)/(float)CLOCKS_PER_SEC);
			delete node;
		}
		else if(input == "print") t.traverse();
		else if(input == "end") {
		cout << endl;
		cout << "Sum: " << duration <<endl;
		cout << "Avg: " << duration/count << endl;
		break;
		}
	}
	
	getchar();
 
	return 0;
}