#include <iostream>
#include <string>
#include <stdlib.h>
#include <fstream>
 
using namespace std;
 
char *filename;
 
struct node{
	int data;
	long left;
	long right;
	long offset;
}; 
 
struct queue{
	node *node;
	queue *next;
	int level;
};
 
queue *head = 0;
queue *tail = 0; 
 
enum child{left,right};
 
 
node* getNode(long offset){
	fstream stream;
	stream.open(filename,ios::in|ios::out|ios::binary);
	stream.seekg(0,ios::end);
	int seek = stream.tellg();
	if(seek==0) {
		stream.close();
		return 0;
	}
	else{
		stream.seekg(offset, stream.beg);
		node *root = new node;
		int data;
		root->offset=offset;
		stream.read((char *)&data, sizeof(int));
		root->data = data;
		stream.seekg(offset+sizeof(int), stream.beg);
		stream.read((char *)&data, sizeof(int));
		root->left = data;
		stream.seekg(offset+2*sizeof(int), stream.beg);
		stream.read((char *)&data, sizeof(int));
		root->right = data;
		stream.close();
		return root;
 
	}
}
 
void write_new_node(int data){
	fstream stream;
	stream.open(filename,ios::in|ios::out|ios::binary);
	stream.seekp(0,ios::end);
	stream.write((char *)&data, sizeof(int));
	int children = -1;
	stream.write((char *)&children, sizeof(int));
	stream.write((char *)&children, sizeof(int));
	stream.close();
}
 
bool hasChild(node *node, child child){
	return node != 0 && (child == child::left ? node->left != -1 : node->right != -1);
}
 
void modifyChild(node *node, child child){
	if(node != 0){
		fstream stream;
		stream.open(filename,ios::in|ios::out|ios::binary);
		stream.seekg(0,ios::end);
		int seek = stream.tellg();
		long offset = node->offset + ((child == child::left ? 1 : 2)*sizeof(int));
		stream.seekp(offset, ios::beg);
		stream.write((char *)&seek,sizeof(int));
		stream.close();
	}
}
 
void insert(int data, node *node){
	if(node == 0){
		write_new_node(data);
		return;
	}
	child child = data>node->data ? child::right : child::left;
	if(hasChild(node, child)) insert(data, getNode(child == child::right ? node->right:node->left));
	else{
		modifyChild(node,child);
		write_new_node(data);
	}
}
 
void find(int data, node *node){
	if(node == 0){ 
		cout<<"Record "<<data<<" does not exist.\n";
		return;
	}
	else if(data == node->data){ 
		cout<<"Record "<<data<<" exists.\n";
		return;
	}
	child child = data>node->data ? child::right : child::left;
	if(hasChild(node, child)) find(data, getNode(child == child::right ? node->right:node->left));
	else cout<<"Record "<<data<<" does not exist.\n";
}
 
void push(long offset, int level){
	queue *q = new queue;
	q->next = 0;
	q->node = getNode(offset);
	q->level = level;
	if(head == 0 && tail == 0) {
			head = q;
			tail = q;
	}
	else{
		tail->next = q;
		tail = q;
	}
}
 
queue* pop(){
	if(tail == head) tail = 0;
	queue *q = head;
	head = head->next;
	q->next = 0;
	return q;
 
}
 
void print(){
	fstream stream;
	stream.open(filename,ios::in|ios::out|ios::binary);
	stream.seekg(0,ios::end);
	int seek = stream.tellg();
	if(seek != 0){
		int currLvl = 1;
		cout<<currLvl<<":";
		push(0, 1);
		while(head != 0 && tail != 0){
			queue *q = pop();
			if(q->level > currLvl){
				cout<<"\n"<<q->level<<":";
				currLvl = q->level;
			}
			cout<<" "<<q->node->data<<"/"<<q->node->offset;
			if(hasChild(q->node, child::left)) push(q->node->left, q->level+1);
			if(hasChild(q->node, child::right)) push(q->node->right, q->level+1);
			delete q;
		}
 
	}
	cout<<"\n";
	stream.close();
}
 
int main(int argc, char *argv[]){
	if(argc != 2){
		cout<<"Usage:bst <filename>";
		return 1;
	}
	filename = argv[1];
	string command = "del ";
	command.append(filename);
	system( command.c_str());
	fstream stream;
	stream.open(filename,ios::app);
	stream.close();
	string input="";
	int val;
	while((cin>>input)){
		if(input == "add"){
			cin>>val;
			insert(val,getNode(0));
		}
		else if(input == "find"){
			cin>>val;
			find(val, getNode(0));
		}
		else if(input == "print") print();
		else if(input == "end") break;
	}
	getchar();
	return 0;
}