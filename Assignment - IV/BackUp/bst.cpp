#include<stdio.h>
#include<iostream>
#include<stdlib.h>
#include<fstream>
#include<string>

using namespace std;

fstream fileReader;

struct binary_node{
	int key;
	long left;
	long right;
};


bool search(int searchNode, long offset){
	cout << "In the search Funtion!!!"<< endl;
	int node=0;
	long left=0, right=0;
	fileReader.seekg(offset,  ios::beg);
	fileReader.read((char *)&node, (int) sizeof( int ));
	cout << "Key for the Node - " << node << endl;
	if(node = 0){
		cout<< "No Elements can be accessed!!!" << endl;
		cout << "searchNode not Found";
		return true;
	}
	if(node = searchNode){
		cout << "Element Exists - Search!!!" << endl;
		return true;
	}
	if (node > searchNode){
		fileReader.seekg((offset + (int)sizeof(int)), ios::beg);
		fileReader.read((char *)&left, (int)sizeof(long));
		cout << "Accessing Left Value - " << left << endl;
		if(left == -1){
			fileReader.seekp(0,  ios::end);
			left = (long)fileReader.tellp();
			fileReader.seekp((offset + (int)sizeof(int)), ios::beg);
			fileReader.write((char *)&left, (int)sizeof(long));
			return false;
		}
		else
			search (searchNode, left);
	}
	else{
		fileReader.seekg((offset+(int)sizeof(int))+sizeof(long),  ios::beg);
		fileReader.read((char *)&right, (int)sizeof(long));
		cout << "Accessing Right Value - " << right << endl;
		search (searchNode, right);
	}

	return false;
}


void add(int newKey, long offset){
	binary_node node;
	int retrieveNode = 0;
	long left=0, right=0;

	/*fileReader.seekg(offset, ios::beg);
	fileReader.read((char *) &retrieveNode, (int)sizeof(int));
*/
	if(retrieveNode == 0){
		cout << "No Nodes exists!!!" << endl;
		cout << "Writing Root Node" << endl;
		node.key = newKey;
		node.left = -1;
		node.right = -1;
		int child = -1;
		cout << "Writing New Node!!!" << endl;
		fileReader.seekp(0, ios::beg);
		fileReader.write((char *)&(newKey), (int)sizeof(int));
		fileReader.write((char *)&(child), (int)sizeof(int));
		fileReader.write((char *)&(child), (int)sizeof(int));
		//fileReader.clear();
		return;
	}

	if(retrieveNode == newKey) {
		cout << "Node " <<newKey << " already exists" << endl;
		return;
	}

	else if(retrieveNode > newKey){
		cout << "Going Left!!!" << endl; 
		fileReader.seekg((offset + (int)sizeof(int)), ios::beg);
		fileReader.read((char *)&left, (int)sizeof(long));
		cout << "Current offset is - " << left << endl;
		if(left == -1){
			fileReader.seekp(0, ios::end);
			left = (long)fileReader.tellp();
			fileReader.seekp((offset + (int)sizeof(int)), ios::beg);
			cout << "Writing offset in left - " << left << endl;
			fileReader.write((char *)&left, (int)sizeof(long));
			fileReader.seekp(0, ios::end);

			node.key = newKey;
			node.left = -1;
			node.right = -1;

			cout << "Writing New Node!!!" << endl;

			fileReader.write((char *)&node.key, (int)sizeof(int));
			fileReader.write((char *)&node.left, (int)sizeof(long));
			fileReader.write((char *)&node.right, (int)sizeof(long));
			//fileReader.clear();
			return;
		}
		else{
			cout << "Searching Again with new Offset - " << left << endl;
			add(newKey, left);
		}
	}
	else {

		cout << "Going Right!!!" << endl;
		fileReader.seekg((offset + (int)sizeof(int)+(int)sizeof(long)), ios::beg);
		fileReader.read((char *)&right, (int)sizeof(long));
		cout << "Current Right Offset value - " << right << endl;
		if(right == -1){
			fileReader.seekp(0, ios::end);
			right = (long)fileReader.tellp();
			cout << "Writing New Right Value - " << right << endl;
			fileReader.seekp((offset + (int)sizeof(int)+(int)sizeof(long)), ios::beg);
			fileReader.write((char *)&right, (int)sizeof(long));
			fileReader.seekp(0, ios::end);

			node.key = newKey;
			node.left = -1;
			node.right = -1;
			cout << "Writing a New Node!!!" << endl;
			fileReader.write((char *)&node.key, (int)sizeof(int));
			fileReader.write((char *)&node.left, (int)sizeof(long));
			fileReader.write((char *)&node.right, (int)sizeof(long));
			fileReader.clear();
			//return;
		}
		else{
			add(newKey, right);
		}
	}



}

int main (int argc, char *argv[]){
	string input;
	string fileName = argv[1];
	
	string command = "del "+fileName;
	cout <<"------ordinary---"<< command << endl;
	const char *deleteCommand = command.c_str();
	cout <<"----char*-----"<< deleteCommand << endl;
	system(deleteCommand);
	cout << "File Deleted!!" << endl;

	fileReader.open(fileName, ios::app);
	fileReader.close();
	//cout << "File Opened!!!" << endl;

	while(1){
		
		cin >> input;

		if(input == "add"){
			//cout << input << endl;
			fileReader.open(fileName, ios::in|ios::out);
			cout << "File Opened!!!" << endl;
			int newKey;
			cin >> newKey;
			add(newKey, 0);
			fileReader.close();
		}
		else if (input == "find"){
			//cout << input << endl;
			int searchKey;
			cin >> searchKey;
			bool searchElement = search(searchKey, 0);
			if(searchElement) 
				cout << "Element Found/Exists!!" << endl;
			else
				cout << "Element Not Found!" << endl;

		}
		else if( input == "print"){
			cout << input << endl;
			fileReader.close();
			return 0;
		}
		else {
			cout << "Please provide a correct command - " << endl;
			cin >> input;
		}
	}

}