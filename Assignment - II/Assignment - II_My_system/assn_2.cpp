/*********************
* Pratyush Swamy
* Assignment - II
* CSC541
***********************/



#include<iostream>
#include<conio.h>
#include<sstream>
#include<fstream>
#include<string.h>

using std::cin;
using std::cout;
using std::ios;
using std::string;

std::fstream studentRec;
std::fstream readAvail;
std::fstream readIndex;

	string studentFile = "";
	string availFile = "";
	string indexFile = "";

/*struct student
{
int recordSize;
long SID;
string last;
string first;
string major;

};*/

int count_index = 0;
int count_avail = 0;

struct avail {
int offset;
int size;
};
avail * pointAvail = new avail[1000];

struct index {
long key;
int offset;
};

index * pointIndex = new index[1000];



void writeIndexToFile()
{
	readIndex.close();
	std::fstream newWriteIndex;
	newWriteIndex.open(indexFile,ios::out|ios::trunc|ios::binary);
	if(count_index==0)
	{
		newWriteIndex.close();
	    return;
	}
	newWriteIndex.seekp(0, ios::beg);
	long offset=newWriteIndex.tellp();
	
	for(int i = 0; i < count_index; i++)
	{
		//cout<<pointIndex[i].key<<" written at : "<<newWriteIndex.tellp()<<", off = "<<offset<<"\n";
		newWriteIndex.write((char*)&pointIndex[i].key,sizeof(pointIndex[i].key));
		offset+=sizeof(long);
		newWriteIndex.seekp(offset,ios::beg);
		//cout<<"its offset written at : "<<newWriteIndex.tellp()<<", off = "<<offset<<"\n";
		newWriteIndex.write((char*)&pointIndex[i].offset,sizeof(pointIndex[i].offset));
		offset+=sizeof(long);
		newWriteIndex.seekp(offset);
	   // cout<<"i = "<<i<<"\n";
	} //end of for
	newWriteIndex.close();

} // end of write index list


int searchIndex(string SID)
	{
		long searchKey = std::stol(SID); 
		int left = 0;
		int mid = 0;
		int recLength = 0; 
		int right = count_index-1;
		long offset = 0;
		char *studentRecord;
		//cout<< "Search Key - "<< searchKey << std::endl;

		while(left <= right)
		{
			mid = (left+right)/2;
			//cout << "--------" << mid << "left -"<< left <<"  right-" << right <<  std::endl;
			if(pointIndex[mid].key==searchKey) 
			{
					offset = pointIndex[mid].offset;
					studentRec.seekg(offset, ios::beg);
					studentRec.read((char*)&recLength,sizeof(int));
					//cout<<"found rec size = "<<recLength<<"\n";
					
					studentRec.seekg(offset+sizeof(int), ios::beg);
					studentRecord=(char*)malloc(recLength+1);
					studentRec.read((char*)studentRecord,recLength);
					studentRecord=studentRecord+'\0';
					//cout<<"found rec : "<<studentRecord<<"\n";
					return 0;
			}//end of if
			else if(searchKey < pointIndex[mid].key)
						right=mid-1;
			else
				left = mid+1;
		} //end of while
	return 1;
	} //end of seachIndex

void sort()
	{
		int i =0;
		index temp;
		//cout << "Number of Elements for SORT in the index FIle " << count_index << std::endl;
		
		for (int j=0; j < count_index; j++)
		{
			temp = pointIndex[j];
			for (i=j-1; (i>=0) && (pointIndex[i].key > temp.key); i--)
			{
			pointIndex[i+1]= pointIndex[i];
			//cout << "Sorting Elements" << std::endl;
			}
		pointIndex[i+1]=temp;
		}
		
		//return count;
}



void addStudent(string str)
{
	string delim = "|";
	string SID = str.substr(0, str.find(delim));
	//cout <<"AddStudent Function - " << SID << std::endl;
	long offset = 0;
	
	if(searchIndex(SID))				//Binary Search in the Index file to look for SID
		{
			long Student_id = std::stol(SID);
			cout <<"This record does not exists!!" << std::endl;
			cout << "write the record into the file." << std::endl;
			int off_set = studentRec.tellp();
			cout<<  "lets find offset-----" << off_set <<std::endl;
			studentRec.seekp(std::ios_base::end);
			
			//offset = ftell(studentRec);
			//cout << "----------"<< str.length() << "----------------\n";
			studentRec << str.size()<< str << std::endl;
			pointIndex[count_index].key = Student_id;
			pointIndex[count_index].offset = off_set;
			count_index++;
			sort();
	}
	else 
		cout << "This record already exists!!" << std::endl;
}



int loadAvailFile()
{
int new_off = 0;
int new_size = 0;
//int count = 0;


while(!readAvail.eof())
{
readAvail.read((char*)&new_off,sizeof(new_off));
readAvail.read((char*)&new_size,sizeof(new_size));
pointAvail[count_avail].offset = new_off;
pointAvail[count_avail].size = new_size;
count_avail++;
}
return 1;
}

int loadIndexFile()
{
long new_key = 0;
int new_off = 0;
//int count = 0;


while(!readIndex.eof())
{
readIndex.read((char*)&new_key,sizeof(new_key));
readIndex.read((char*)&new_off,sizeof(new_off));
pointIndex[count_index].key = new_key;
pointIndex[count_index].offset = new_off;
count_index++;
}

return 1;
}

void displayIndex()
	{
		//int num_elements  = sizeof(pointIndex)/sizeof(pointIndex[0]);
		//cout << "Number of Elements in the index FIle " << count_index << std::endl;
		//cout << "Displaying Index" << std::endl;

		for(int i = 0; i < count_index; i++)
		{
			cout << pointIndex[i].key << " " << pointIndex[i].offset << std::endl;
		}
	
	}
void end()
	{
		//cout << "GOING TO write file" ;
		writeIndexToFile();

	}

int main(int argc,char *argv[])
{

	studentFile = argv[1];
	availFile = studentFile+".avl";
	indexFile = studentFile+".idx";
	string input="";
	studentRec.open(studentFile, ios::in|ios::out|ios::binary|ios::app);
	readAvail.open(availFile, ios::in|ios::out|ios::binary|ios::app);
	readIndex.open(indexFile, ios::in|ios::out|ios::binary|ios::app);

	int check = loadIndexFile();
	if(check == 1){cout << "Successfully Loaded the Index File!";}
	else{cout<<"Unable to load the Index File!";}

	int checkAvail = loadAvailFile();
	if(checkAvail == 1){cout << "\nSuccessfully Loaded the Availability File!";}
	else{cout<<"\nUnable to load the Availability File!"<<"\n";}

	while(cin >> input)
		{
		if(input == "add")
			{
			cin >> input;
			addStudent(input);
			displayIndex();
			}
		else if(input == "find")
			{
			cin >> input;
			string delim = "|";
			string SID = input.substr(0, input.find(delim));
			searchIndex(SID);
		
		} 
		
	else if(input=="del")
	{
	cout << "Delete Functionality Not Added";
	cout <<"Also, Availability List is not done";
	//cin >> input;
	//	del(input);
	}
	else {
		if(input == "end")
		{
			cout << "Complete Functionality for End not added!!!";
			end();
		}
	}
	} // end of while

}



