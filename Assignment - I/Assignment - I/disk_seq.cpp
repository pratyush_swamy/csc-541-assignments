/*
# CSC 541 - Assignment I
# Pratyush Swamy
# Unity ID - pswamy
# Disk Based Sequential Search
*/
#include <windows.h>
#include <iostream>
#include <fstream>
#include <new>

using std::cout;
using std::ifstream;
using std::ios;
using std::streampos;

int main( int argc, char *argv[] )
{
	ifstream		fseek, fkey;						 // Input file stream
	int				val;								 // Current input value
	int				seek_elements;			   			 //No. of elements in the seek.pc.db
	int				no_of_hits;							 // No.of hits on each element of seek.pc.db
	streampos		seek_size;

	/* Finding the size of seek.pc.db */
	fseek.open( "seek.pc.db", ios::in | ios::binary );		
	seek_size = fseek.tellg();
	fseek.seekg( 0, ios::end );
	seek_size = fseek.tellg() - seek_size;
	//cout << "Size of seek.pc.db == " << seek_size << "\n";
	seek_elements = (int) seek_size / (int) sizeof( int );
	//cout<< "No. of Elements in seek.pc.db == " << seek_elements << "\n";

	/* Reading seek.pc.db to an appropriate sized array*/
	fseek.seekg( 0, ios::beg );
	fseek.read( (char *) &val, (int) sizeof( int ) );
	int	counterSeek = 0;
	int * seek;
	seek = new int [seek_elements];

	while( !fseek.eof() )
	{
		seek[counterSeek] = (int) val;
		fseek.read( (char *) &val, (int) sizeof( int ) );
		counterSeek++;

	}

	//cout << "No. of elements in key.pc= " << counterSeek << "\n";

	int Hit[10000][2];
	
	SYSTEMTIME  beg;      // Start time
	GetLocalTime( &beg );

	fkey.open( "key.pc.db", ios::in | ios::binary );
	fkey.seekg( 0, ios::beg );
	/* Reading the values from seek array into another array (Hit) and then comparing it with 
	element from the binary file key.pc.db*/
	for (int i = 0; i < 10000; i++)
	{
		Hit[i][0] = seek[i];
		int counterHit = 0;
		int val = 0;
		for(int j = 0; j < 5000; j++)
		{
			//Reading from the disk indiviual elements of key.pc.db
			fkey.seekg( j * (int) sizeof( int ), ios::beg);
			fkey.read( (char *) &val, (int) sizeof( int ) );

			if(Hit[i][0] == (int) val)
			{
				counterHit++;
			}
		}
		Hit[i][1] = counterHit;

	}
	SYSTEMTIME  end;      // End time
	GetLocalTime( &end );

	printf( "%02d:%02d:%02d:%06d\n", beg.wHour,
		beg.wMinute, beg.wSecond, beg.wMilliseconds * 1000 );

	printf( "%02d:%02d:%02d:%06d\n", end.wHour,
		end.wMinute, end.wSecond, end.wMilliseconds * 1000 );
	for ( int i = 0; i < 10000; i++ )
	{
		printf("%6d: %d\n", Hit[i][0], Hit[i][1]);
	}	

	/*Closing Streams*/
	fkey.close();
	fseek.close();
	delete [] seek;
	return 1;
}