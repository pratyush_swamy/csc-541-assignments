/*
# CSC 541 - Assignment I
# Pratyush Swamy
# Unity ID - pswamy
# Memory Based Sequential Search
*/	

#include <windows.h>
#include <iostream>
#include <fstream>
#include <new>

using std::cout;
using std::ifstream;
using std::ios;
using std::ofstream;
using std::streampos;

int main( int argc, char *argv[] )
{
	ifstream  fseek, fkey;        // Input file stream
	int       val;				// Current input value
	int		seek_elements;			//No. of elements in the seek.pc.db
	int		key_elements;		//No. of elements in the key.pc.db
	int		no_of_hits;			// No.of hits on each element of seek.pc.db
	streampos		seek_size, key_size;

	/* Finding the number of elements in seek.pc.db */
	fseek.open( "seek.pc.db", ios::in | ios::binary );
	seek_size = fseek.tellg();
	fseek.seekg( 0, ios::end );
	seek_size = fseek.tellg() - seek_size;
	seek_elements = (int) seek_size / (int) sizeof( int );

	/* Storing the elements of seek.pc.db in an array */
	fseek.seekg( 0, ios::beg );
	fseek.read( (char *) &val, (int) sizeof( int ) );
	int	counterSeek = 0;
	int * seek;
	seek = new int [seek_elements];
	while( !fseek.eof() ) 
	{
		seek[counterSeek] = (int) val;
		fseek.read( (char *) &val, (int) sizeof( int ) );
		counterSeek++;
	}

	/* Finding the number of elements in key.pc.db */
	SYSTEMTIME  beg;      // Start time
	GetLocalTime( &beg );

	fkey.open( "key.pc.db", ios::in | ios::binary );
	key_size = fkey.tellg();
	fkey.seekg( 0, ios::end );
	key_size = fkey.tellg() - key_size;
	key_elements = (int) key_size / (int) sizeof( int );

	
	/* Storing the elements of key.pc.db in an array */
	fkey.seekg( 0, ios::beg );
	fkey.read( (char *) &val, (int) sizeof( int ) );
	int * key;
	key = new int [key_elements];
	int counter = 0;
	while( !fkey.eof() ) 
	{
		key[counter] = (int) val;
		fkey.read( (char *) &val, (int) sizeof( int ) );
		counter++;
	}

	/*Storing the elements of seek.pc.db in another array (Hit) and comparing the elements to the key.pc.db*/
	int Hit[10000][2];
	for (int i = 0; i < seek_elements; i++)
	{
		Hit[i][0] = seek[i];
		int counterHit = 0;
		for(int j = 0; j < 5000; j++)
		{
			if(Hit[i][0] == key[j])
			{
				counterHit++;
			}
		}
		Hit[i][1] = counterHit;
	}

	SYSTEMTIME  end;      // End time
	GetLocalTime( &end );

	printf( "%02d:%02d:%02d:%06d\n", beg.wHour,
		beg.wMinute, beg.wSecond, beg.wMilliseconds * 1000 );

	printf( "%02d:%02d:%02d:%06d\n", end.wHour,
		end.wMinute, end.wSecond, end.wMilliseconds * 1000 );
	for ( int i = 0; i < 10000; i++ )
	{
		printf("%6d: %d\n", Hit[i][0], Hit[i][1]);
	}	

	/*Closing Streams*/

	fkey.close();
	fseek.close();
	delete [] key;
	delete [] seek;
	return 1;
}