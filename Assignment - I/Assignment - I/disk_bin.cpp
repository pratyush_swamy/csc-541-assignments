/*
# CSC 541 - Assignment I
# Pratyush Swamy
# Unity ID - pswamy
# Disk Based Binary Search
*/
#include <windows.h>
#include <iostream>
#include <fstream>
#include <new>

using std::cout;
using std::ifstream;
using std::ios;
using std::ofstream;
using std::streampos;

int main( int argc, char *argv[] )
{
	int			  val;					// Current input value
	int			seek_elements;			//No. of elements in the seek.pc.db
	int			key_elements;			//No. of elements in key.pc.db
	int			no_of_hits;				// No.of hits on each element of seek.pc.db
	streampos	seek_size, key_size;
	ifstream	fseek, fkey;			// Input file stream
	
	/* Calculating the number of elements in key.pc.db */
	fkey.open( "key.pc.db", ios::in | ios::binary );
	key_size = fkey.tellg();
	fkey.seekg( 0, ios::end );
	key_size = fkey.tellg() - key_size;
	key_elements = (int) key_size / (int) sizeof( int );
	fkey.close();
	//cout<< "No. of Elements in key.pc.db == " << key_elements << "\n";
	//cout << "Size of key.pc.db == " << key_size << "\n";
	
	
	/*** Calculating the number of elements in seek.pc.db ***/
	fseek.open( "seek.pc.db", ios::in | ios::binary );
	
	seek_size = fseek.tellg();
	fseek.seekg( 0, ios::end );
	seek_size = fseek.tellg() - seek_size;
	seek_elements = (int) seek_size / (int) sizeof( int );

	/*cout << "Size of seek.pc.db == " << seek_size << "\n";
	cout<< "No. of Elements in seek.pc.db == " << seek_elements << "\n";*/
	
	/*** Loading seek.pc.db into array seek ***/
	fseek.seekg( 0, ios::beg );
	fseek.read( (char *) &val, (int) sizeof( int ) );
	int	counterSeek = 0;
	//int seek[10000];
	int * seek;
	seek = new int [seek_elements];
	while( !fseek.eof() ) 
	{
		seek[counterSeek] = (int) val;
		fseek.read( (char *) &val, (int) sizeof( int ) );
		counterSeek++;
	}
	//cout << "No. of elements in seek array= " << counterSeek << "\n";
	
	SYSTEMTIME  beg;      // Start time
	GetLocalTime( &beg );
	
	fkey.open( "key.pc.db", ios::in | ios::binary );
	fkey.seekg( 0, ios::beg );

	/* Reading the values from seek array and then comparing it with 
	element from the binary file key.pc.db*/

	int hit[10000] = {0};			//To store the value of HIT or MISS
	for(int i = 0; i < seek_elements; i++)
	{
		int lowerbound = 0;
		int upperbound = key_elements - 1;
		int position = (int) ((lowerbound + upperbound) / 2);
		/* Reading specific elements from key.pc.db */
		fkey.seekg(position * (int) sizeof(int), ios::beg);
		fkey.read( (char *) &val, (int) sizeof( int ) );
		
		while ((int) val != seek[i] && lowerbound <= upperbound)
		{
			if((int) val > seek[i])
			{ upperbound = position - 1; }
			else
			{ lowerbound = position + 1; }

			position = (int) ((lowerbound + upperbound)/2);
			fkey.seekg(position * (int) sizeof(int), ios::beg);
			fkey.read( (char *) &val, (int) sizeof( int ) );
			
		}

		if ((int) val == seek[i]) {hit[i] = 1;}
	}
	
	SYSTEMTIME  end;      // End time
	GetLocalTime( &end );

	printf( "%02d:%02d:%02d:%06d\n", beg.wHour,
		beg.wMinute, beg.wSecond, beg.wMilliseconds * 1000 );

	printf( "%02d:%02d:%02d:%06d\n", end.wHour,
		end.wMinute, end.wSecond, end.wMilliseconds * 1000 );
	for ( int i = 0; i < 10000; i++ )
	{
	printf( "%6d: %d\n", seek[ i ], hit[ i ] );
	}

	/* Closing Streams */
	fkey.close();
	fseek.close();
	delete [] seek;
	return 1;
}