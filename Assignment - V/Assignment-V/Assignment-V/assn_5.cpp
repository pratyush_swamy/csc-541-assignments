/*
Name - Pratyush Swamy
Unity Id - pswamy
Assignment - V
Topic - Chained Hashing
Subject - CSC541
*/
#include <stdlib.h>
#include <string>
#include <fstream>
#include <iostream>
#include <sstream>

using std::string;
using std::cin;
using std::cout;
using std::fstream;
using std::endl;
using std::getline;
using std::ios;
using std::cerr;
using std::stringstream; 
string fileName;
string indexFile;
static int CHAIN_SIZE = 1001;

// Financial record structure
struct f_rec {
	int    num;		// Transaction number
	float  amt;		//Amount of the transaction
	char   type;	//Transaction type (D=debit, C=credit)
	int    acct;	// Account used for transaction
	int    hour;	// Hour of transaction (0-23)
	int    min;		// Minute of transaction (0-59)
	int    day;		// Day of transaction (0-364)
	int    year;	// Year of transaction (0000-9999)
};


struct ch_node {   // Chained hash table node
	int   k;       // Key of record node represents
	long  rec;     // Offset of corresponding record in database file
	long  next;    // Offset of next node in this chain (-1 for none)
};    

f_rec * readDataFile(long offset){
	f_rec * node = new f_rec();
	fstream readNode;
	readNode.open(fileName, ios::in|ios::out|ios::binary);
	readNode.seekg(offset, ios::beg);
	readNode.read(reinterpret_cast<char*>(node), sizeof(f_rec));
	readNode.close();
	return node;
}

ch_node * readIndexKey(long offset) {
	ch_node * indexValue = new ch_node();
	fstream readIndex;
	readIndex.open(indexFile, ios::in|ios::out|ios::binary);
	readIndex.seekg(offset, ios::beg);
	readIndex.read(reinterpret_cast<char*>(indexValue), sizeof(ch_node));
	readIndex.close();
	return indexValue;
}

long writeKey(f_rec * record){
	fstream writeKey;
	writeKey.open(fileName, ios::in|ios::out|ios::binary);
	writeKey.seekp(0, ios::end);
	long offset = writeKey.tellp();
	writeKey.write(reinterpret_cast<char*>(record), sizeof(f_rec));
	writeKey.close();
	return offset;
}

void addKey(f_rec * insert, ch_node * index, long currentIndexOffset){

	fstream writeIndexFile;
	writeIndexFile.open(indexFile, ios::in|ios::out|ios::binary);

	if(index->k == insert->num){
		cout << "Record " << insert->num << " is a duplicate." << endl;
	}
	else if(index->k == -1 && index->next == -1){
		long offset = writeKey(insert);
		index->k = insert->num;
		index->rec = offset;
		index->next = -1;
		writeIndexFile.seekp(currentIndexOffset, ios::beg);
		writeIndexFile.write(reinterpret_cast<char*>(index), sizeof(ch_node));
	}
	else if(index->k!= insert->num && index->next == -1){
		//Writing the record in the Record Database
		long offset = writeKey(insert); // Offset of the record in the Database File

		ch_node *indexNode = new ch_node();
		indexNode->k = insert->num;
		indexNode->rec = offset;
		indexNode->next = -1;

		//Writing the value of newKey in Index File
		writeIndexFile.seekp(0, ios::end);
		long updateNext = writeIndexFile.tellp();	// Offset (in Index File) of the Index Key for updating the current Node
		writeIndexFile.write(reinterpret_cast<char*>(indexNode), sizeof(ch_node));

		//Updating the record for the value of "next"in Current Key in IndexFile
		writeIndexFile.seekp(currentIndexOffset, ios::beg);
		index->next = updateNext;
		writeIndexFile.write(reinterpret_cast<char*>(index), sizeof(ch_node));
	}
	else{
		writeIndexFile.close();
		addKey(insert, readIndexKey(index->next), index->next);
	}
	writeIndexFile.close();
}

bool deleteFromIndex(ch_node * previous, ch_node * current , long prevOff, long currOff, int deleteKey){
	fstream writeIndex;
	writeIndex.open(indexFile, ios::in|ios::out|ios::binary);
		//If the record is found in the Index File
		if(current->k == deleteKey){
			previous->next = current->next;
			writeIndex.seekp(prevOff, ios::beg);
			writeIndex.write(reinterpret_cast<char*>(previous), sizeof(ch_node));
			return true;
		}
		else if(current->next == -1) {
			return false;
		}
		return deleteFromIndex(current, readIndexKey(current->next), currOff, current->next, deleteKey);
	}
	
bool searchIndexFile(int searchKey, ch_node * key){
	f_rec * dispRecord = new f_rec();
	if(key->k == searchKey){
		dispRecord = readDataFile(key->rec);
		cout << dispRecord->num << " " << dispRecord ->amt << " "<< dispRecord->type
			<< " " << dispRecord->acct << " " << dispRecord->hour << " " << dispRecord->min 
			<< " " << dispRecord->day << " " << dispRecord->year << endl;
		return true;
	}
	else if(key->next == -1)
		return false;
	else
		return searchIndexFile(searchKey, readIndexKey(key->next));
}


//Printing the Whole Database File
//void printDatabaseFile(){
//	fstream printDatabase;
//	f_rec * record = new f_rec();
//	long offset = 0;
//	printDatabase.open(fileName, ios::in|ios::out|ios::binary);
//	if(printDatabase.fail()){
//		cout << "File Cannot be opened!" << endl; 
//	}
//	printDatabase.seekg(0, ios::beg);
//	while(!printDatabase.eof()){
//		offset = printDatabase.tellg();
//		printDatabase.read(reinterpret_cast<char*>(record), sizeof(f_rec));
//		cout << "Offset of this Record - " << offset << endl;
//		cout << "Transaction ID - " << record->num <<endl;
//		cout << "Amount Of Transaction - " << record->amt << endl;
//		cout << "Transaction Type - " << record->type << endl;
//		cout << "Account No. - " << record->acct << endl;
//		cout << "Hour of Transaction - " << record->hour << endl;
//		cout << "Minute of Day - " << record->min << endl;
//		cout << "Day of transaction - " << record->day << endl;
//		cout << "Year of Transaction - " << record->year << endl;
//		cout << "-------------------------" << endl;
//	}
//	printDatabase.close();
//}


// Printing the Whole Index File
void printIndexFile(){
	for(int i = 0 ; i < CHAIN_SIZE ; i++){
		ch_node * index = readIndexKey(i * sizeof(ch_node));
		cout << i << ": ";
		while(index->next != -1){
			if(index->k != -1 ){
			cout << index->k << "/" << index->rec << " ";
			}
			index = readIndexKey(index->next);
		}
		if(index->k != -1){
			cout << index->k << "/" << index->rec << endl;
		}
		else
		cout  << endl;
	}
}

int main (int argc, char * argv[]){
	if(argc != 3){
		cerr << "Usage: Chained Hashing : File Missing!" << endl;
	}

	string input = "";
	indexFile = argv[1];
	fileName = argv[2];


	/*******************************/
	fstream check, checkdb;
	check.open(indexFile, ios::app);
	check.close();
	checkdb.open(fileName, ios::app);
	checkdb.close();
	/*******************************/
	check.open(indexFile, ios::out|ios::in|ios::binary);
	check.seekp(0, ios::end);
	long checkOffset = check.tellp();
	if( checkOffset == 0){
		ch_node * node = new ch_node();
		node->k = -1;
		node->next = -1;
		for (int i = 0; i < CHAIN_SIZE; i++){
			check.write(reinterpret_cast<char*>(node), sizeof(ch_node));
		}
		long final_offset = check.tellp();
		//cout << "Final Output - " << final_offset << endl;
		check.close();
	}

	while (cin >> input){
		if(input == "add"){
			f_rec * record = new f_rec;
			cin >> record->num;
			cin>> record->amt;
			cin >> record->type;
			cin >>  record->acct;
			cin >>  record->hour;
			cin >> record->min;
			cin >> record->day;
			cin >> record->year;

			int hash = record->num % CHAIN_SIZE;
			//cout << "value of Hash - " << hash << endl;
			long indexOffset = hash * sizeof(ch_node);
			addKey(record, readIndexKey(indexOffset), indexOffset);

		}
		else if(input == "find"){
			int searchKey = 0;
			cin >> searchKey;
			int hash = searchKey % CHAIN_SIZE;
			long offset = hash * sizeof(ch_node);
			if(!searchIndexFile(searchKey, readIndexKey(offset))){
				cout << "Record " << searchKey << " does not exist." << endl;
			}
		}
		else if(input == "delete"){
			int delKey = 0 ;
			cin >> delKey;
			int hash = delKey % CHAIN_SIZE;
			ch_node * previous = new ch_node();
			previous->k = -1;
			previous->next = -1;
			previous->rec = -1;
			long offset = hash * sizeof(ch_node); 
			if(!deleteFromIndex(previous, readIndexKey(offset), offset, offset, delKey)){
				cout << "Record " << delKey << " does not exist" << endl; 
			}
		}
		else if (input == "print"){
			printIndexFile();
			//printDatabaseFile();
		}
		else if (input == "end"){
			break;
		}

	}
}